package com.deposco.ias.handlers;

import com.deposco.ias.model.CloudwatchAlarmRequest;

public interface CloudwatchAlarmHandler {

	public void handleAlarm(CloudwatchAlarmRequest alarmRequest) throws HandlerException;
}
