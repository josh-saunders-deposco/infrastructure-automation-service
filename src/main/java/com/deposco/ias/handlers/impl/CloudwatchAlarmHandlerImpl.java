package com.deposco.ias.handlers.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.deposco.ias.brokers.LogQueryBroker;
import com.deposco.ias.brokers.aws.AWSException;
import com.deposco.ias.brokers.aws.AWSSESBroker;
import com.deposco.ias.brokers.impl.SlackNotificationBroker;
import com.deposco.ias.handlers.CloudwatchAlarmHandler;
import com.deposco.ias.handlers.HandlerException;
import com.deposco.ias.model.CloudwatchAlarmRequest;
import com.deposco.ias.notification.EmailFactory;
import com.deposco.ias.notification.NotificationException;
import com.deposco.ias.notification.SlackNotificationFactory;
import com.deposco.ias.notification.SuppressNotificationException;
import com.deposco.ias.notification.model.Email;
import com.deposco.ias.notification.model.SlackNotification;

@Component
public class CloudwatchAlarmHandlerImpl implements CloudwatchAlarmHandler {
	private static final Logger LOG = LoggerFactory.getLogger(CloudwatchAlarmHandlerImpl.class);

	@Autowired
	private AWSSESBroker sesBroker;
	@Autowired
	private EmailFactory emailFactory;
	@Autowired
	private SlackNotificationBroker slackNotificationBroker;
	@Autowired
	private SlackNotificationFactory slackFactory;
	@Autowired
	private LogQueryBroker logQueryBroker;

	@Override
	public void handleAlarm(CloudwatchAlarmRequest alarmRequest) throws HandlerException {
		try {
			Email email = emailFactory.create(alarmRequest.getMessage().getAlarmName().toLowerCase(), alarmRequest.getTimestamp());
			sesBroker.sendEmails(email.getSubject(), email.getBody(), email.getToAddresses(), email.getFromAddress());

			SlackNotification slackNotification = slackFactory.create(alarmRequest.getMessage().getAlarmName().toLowerCase(), alarmRequest.getTimestamp());
			slackNotificationBroker.sendSlackNotification(slackNotification);
		}
		catch (AWSException e) {
			LOG.error("handleAlarm() :: Exception :: " + e, e);
			throw new HandlerException(e);
		}
		catch (SuppressNotificationException e) {
			LOG.info("handleAlarm() :: Suppressing alarm notification");
		}
		catch (NotificationException e) {
			LOG.error("handleAlarm() :: Exception while generating email content:: " + e, e);
			throw new HandlerException(e);
		}
	}
}
