package com.deposco.ias.handlers.impl;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import jersey.repackaged.com.google.common.collect.ImmutableList;
import jersey.repackaged.com.google.common.collect.ImmutableMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.deposco.ias.brokers.PrinterBackupBroker;
import com.deposco.ias.brokers.aws.AWSException;
import com.deposco.ias.brokers.aws.AWSS3Broker;
import com.deposco.ias.brokers.aws.AWSSQSBroker;
import com.deposco.ias.brokers.aws.AWSUserBroker;
import com.deposco.ias.handlers.HandlerException;
import com.deposco.ias.handlers.PrintServerProvisioningHandler;
import com.deposco.ias.model.PrintServerSecurityCredentials;
import com.deposco.ias.model.ProvisionPrintServerRequest;
import com.deposco.ias.model.ProvisionPrintServerResponse;
import com.deposco.ias.model.aws.CreateAWSSQSQueueRequest;
import com.deposco.ias.model.aws.CreateAWSSQSQueueResponse;
import com.deposco.ias.model.aws.CreateAWSUserAccessKeyRequest;
import com.deposco.ias.model.aws.CreateAWSUserAccessKeyResponse;
import com.deposco.ias.model.aws.CreateAWSUserPolicyRequest;
import com.deposco.ias.model.aws.CreateAWSUserRequest;
import com.deposco.ias.model.aws.CreateAWSUserResponse;
import com.deposco.ias.model.aws.GetAWSSQSQueueAttributesRequest;
import com.deposco.ias.model.aws.GetAWSSQSQueueAttributesResponse;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@Component
public class PrintServerProvisioningHandlerImpl implements PrintServerProvisioningHandler {
	private static final Logger LOG = LoggerFactory.getLogger(PrintServerProvisioningHandlerImpl.class);
	private static final String QUEUE_ARN_ATTRIBUTE_KEY = "QueueArn";
	private static final String S3_BUCKET = "infrastructure-automation-service";

	@Autowired
	private AWSSQSBroker awsSqsBroker;
	@Autowired
	private AWSUserBroker awsUserBroker;
	@Autowired
	private AWSS3Broker awsS3Broker;
	@Autowired
	private PrinterBackupBroker printerBackupBroker;

	@Override
	public ProvisionPrintServerResponse provisionPrinter(ProvisionPrintServerRequest request) throws HandlerException {
		ProvisionPrintServerResponse response = new ProvisionPrintServerResponse();
		response.setCompany(request.getCompanyCode());
		response.setFacility(request.getFacilityCode());
		// Generate the queue name based on the company code, facility code, and server code
		String sqsQueueName = generateQueueName(request);
		response.setQueueName(sqsQueueName);

		// Create the queue; If the queue already exists, the api will return normally with the existing URL
		CreateAWSSQSQueueResponse createQueueResponse = createSQSQueue(buildAWSCreateQueueRequest(sqsQueueName));
		String queueUrl = createQueueResponse.getQueueUrl();

		// Get the attributes of the queue so we have access to the ARN number for creating a policy
		GetAWSSQSQueueAttributesResponse getQueueAttributesResponse = getSQSQueueAttributes(buildAWSGetQueueAttributesRequest(queueUrl));

		// create a user, if it doesn't already exist for the company, that can access the queue
		CreateAWSUserResponse createUserResponse = createUser(buildAWSCreateUserRequest(request));
		String userName = createUserResponse.getUser().getUserName();

		// create a new policy associated with the queue and user we created
		createUserPolicyIfNeeded(buildAWSCreateUserPolicyRequest(userName,
				getQueueAttributesResponse.getAttributes() != null ? getQueueAttributesResponse.getAttributes().get(QUEUE_ARN_ATTRIBUTE_KEY) : null, request));

		// check to see if security keys exists
		PrintServerSecurityCredentials securityCredentials = getPrintServerSecurityCredentials(request);

		// if the security keys don't exist, create them and store them on S3 for future use
		if (securityCredentials == null || securityCredentials.getAccessKey() == null && securityCredentials.getSecretKey() == null) {
			// create Access Key for user or get the existing one
			CreateAWSUserAccessKeyResponse accessKeyResponse = createAccessKeyForUser(buildCreateUserAccessKeyRequest(userName));
			securityCredentials = new PrintServerSecurityCredentials();
			securityCredentials.setAccessKey(accessKeyResponse.getAccessKey().getAccessKeyId());
			securityCredentials.setSecretKey(accessKeyResponse.getAccessKey().getSecretAccessKey());
			createPrintServerSecurityCredentialsOnS3(request, accessKeyResponse.getAccessKey().getAccessKeyId(), accessKeyResponse.getAccessKey().getSecretAccessKey());
		}

		// set the security credentials in the response to be sent back to the client AKA print server
		response.setAccessKey(securityCredentials.getAccessKey());
		response.setSecretKey(securityCredentials.getSecretKey());

		// create the backup folder structure if needed
		printerBackupBroker.createBackupDirectoryIfNeeded(response.getCompany(), response.getQueueName());

		return response;
	}

	private void createPrintServerSecurityCredentialsOnS3(ProvisionPrintServerRequest request, String accessKey, String secretKey) throws HandlerException {
		LOG.info("createPrintServerConfigOnS3() :: called with request :: " + request + " :: access keys :: " + ((accessKey != null && secretKey != null) ? "present" : "missing"));
		File file = null;

		try {
			file = new File(createPrintServerConfigFile(request, accessKey, secretKey));
			StringBuilder remoteFilePath = new StringBuilder();
			remoteFilePath.append("print-server-config/");
			remoteFilePath.append(request.getCompanyCode() + "/");
			remoteFilePath.append("keys.conf");
			awsS3Broker.createFileAtPath(S3_BUCKET, file, remoteFilePath.toString(), true);
		}
		catch (AWSException e) {
			LOG.error("createPrintServerConfigOnS3() :: AWSException :: " + e);
			throw new HandlerException(e);
		}
		finally {
			file.delete();
		}
		LOG.info("createPrintServerConfigOnS3() :: success");
	}

	private PrintServerSecurityCredentials getPrintServerSecurityCredentials(ProvisionPrintServerRequest request) throws HandlerException {
		LOG.info("getPrintServerSecurityCredentials() :: called with request :: " + request);

		PrintServerSecurityCredentials response = null;
		File file = null;

		try {
			file = new File("/tmp/" + request.getCompanyCode() + ".config.s3");
			StringBuilder remoteFilePath = new StringBuilder();
			remoteFilePath.append("print-server-config/");
			remoteFilePath.append(request.getCompanyCode() + "/");
			remoteFilePath.append("keys.conf");
			awsS3Broker.populateFileUsingFileAtPath(S3_BUCKET, remoteFilePath.toString(), file);

			if (file.length() != 0) {
				BufferedReader reader = new BufferedReader(new FileReader(file));
				String timestamp = reader.readLine();
				String accessKey = reader.readLine().split("=")[1];
				String secretKey = reader.readLine().split("=")[1];
				reader.close();

				if (accessKey != null && accessKey.length() > 0 && secretKey != null && secretKey.length() > 0) {
					LOG.info("getPrintServerSecurityCredentials() :: existing keys found");
					response = new PrintServerSecurityCredentials();
					response.setAccessKey(accessKey);
					response.setSecretKey(secretKey);
				}
				else {
					LOG.info("getPrintServerSecurityCredentials() :: existing keys NOT found");
				}
			}
			else {
				LOG.info("getPrintServerSecurityCredentials() :: existing keys NOT found");
			}
		}
		catch (AWSException e) {
			LOG.error("getPrintServerSecurityCredentials() :: AWSException :: " + e);
			throw new HandlerException(e);
		}
		catch (Exception e) {
			LOG.error("getPrintServerSecurityCredentials() :: Exception :: " + e);
			throw new HandlerException(e);
		}
		finally {
			file.delete();
		}

		LOG.info("getPrintServerSecurityCredentials() :: success");
		return response;
	}

	private CreateAWSUserAccessKeyResponse createAccessKeyForUser(CreateAWSUserAccessKeyRequest request) throws HandlerException {
		LOG.info("createAccessKeyForUser() :: called with request :: " + request);
		CreateAWSUserAccessKeyResponse response = null;

		try {
			response = awsUserBroker.createAccessKeyForUser(request);
		}
		catch (AWSException e) {
			LOG.error("createAccessKeyForUser() :: AWSException :: " + e);
			throw new HandlerException(e);
		}

		LOG.info("createAccessKeyForUser() :: returning response :: " + response);
		return response;
	}

	private GetAWSSQSQueueAttributesResponse getSQSQueueAttributes(GetAWSSQSQueueAttributesRequest request) throws HandlerException {
		LOG.info("getSQSQueueAttributes() :: called with request :: " + request);
		GetAWSSQSQueueAttributesResponse response = null;

		try {
			response = awsSqsBroker.getQueueAttributes(request);
		}
		catch (AWSException e) {
			LOG.error("getSQSQueueAttributes() :: AWSException :: " + e);
			throw new HandlerException(e);
		}

		LOG.info("getSQSQueueAttributes() :: returning response :: " + response);
		return response;
	}

	private void createUserPolicyIfNeeded(CreateAWSUserPolicyRequest request) throws HandlerException {
		LOG.info("createUserPolicyIfNeeded() :: called with request :: " + request);
		try {
			awsUserBroker.createUserPolicyIfNeeded(request);
		}
		catch (AWSException e) {
			LOG.error("createUserPolicy() :: AWSException :: " + e);
			throw new HandlerException(e);
		}
		LOG.info("createUserPolicyIfNeeded() :: success");
	}

	private CreateAWSUserResponse createUser(CreateAWSUserRequest request) throws HandlerException {
		LOG.info("createUser() :: called with request :: " + request);
		CreateAWSUserResponse response = null;

		try {
			response = awsUserBroker.createUserIfNeeded(request);
		}
		catch (AWSException e) {
			LOG.error("createUser() :: AWSException :: " + e);
			throw new HandlerException(e);
		}

		LOG.info("createUser() :: returning response :: " + response);
		return response;
	}

	private CreateAWSSQSQueueResponse createSQSQueue(CreateAWSSQSQueueRequest request) throws HandlerException {
		LOG.info("createSQSQueue() :: called with request :: " + request);
		CreateAWSSQSQueueResponse response = null;

		try {
			response = awsSqsBroker.createQueue(request);
		}
		catch (AWSException e) {
			LOG.error("createSQSQueue() :: AWSException :: " + e);
			throw new HandlerException(e);
		}

		LOG.info("createSQSQueue() :: returning response :: " + response);
		return response;
	}

	private GetAWSSQSQueueAttributesRequest buildAWSGetQueueAttributesRequest(String queueUrl) {
		GetAWSSQSQueueAttributesRequest awsRequest = new GetAWSSQSQueueAttributesRequest();
		awsRequest.setQueueUrl(queueUrl);
		List<String> attributeNames = new ArrayList<String>();
		attributeNames.add(QUEUE_ARN_ATTRIBUTE_KEY);
		awsRequest.setAttributeNames(attributeNames);
		return awsRequest;
	}

	private CreateAWSSQSQueueRequest buildAWSCreateQueueRequest(String queueName) {
		CreateAWSSQSQueueRequest awsRequest = new CreateAWSSQSQueueRequest();
		awsRequest.setQueueName(queueName);
		return awsRequest;
	}

	private CreateAWSUserAccessKeyRequest buildCreateUserAccessKeyRequest(String userName) {
		CreateAWSUserAccessKeyRequest request = new CreateAWSUserAccessKeyRequest();
		request.setUserName(userName);
		return request;
	}

	private String generateQueueName(ProvisionPrintServerRequest request) {
		StringBuilder queueName = new StringBuilder();
		queueName.append(request.getCompanyCode());

		if (request.getFacilityCode() != null && !"".equals(request.getFacilityCode())) queueName.append("_" + request.getFacilityCode());

		if (request.getServerCode() != null && !"".equals(request.getServerCode())) queueName.append("_" + request.getServerCode());

		queueName.append("_PrintJobs");
		return queueName.toString();
	}

	private CreateAWSUserRequest buildAWSCreateUserRequest(ProvisionPrintServerRequest request) {
		CreateAWSUserRequest awsRequest = new CreateAWSUserRequest();
		StringBuilder userName = new StringBuilder();
		userName.append("cups-");
		userName.append(request.getCompanyCode());
		awsRequest.setUserName(userName.toString());
		return awsRequest;
	}

	private CreateAWSUserPolicyRequest buildAWSCreateUserPolicyRequest(String awsUserName, String sqsQueueArn, ProvisionPrintServerRequest request) throws HandlerException {
		CreateAWSUserPolicyRequest awsRequest = new CreateAWSUserPolicyRequest();
		awsRequest.setUserName(awsUserName);
		StringBuilder policyName = new StringBuilder();
		policyName.append("sqs-policy-");
		policyName.append(request.getCompanyCode());
		policyName.append((request.getFacilityCode() != null && !request.getFacilityCode().equals("")) ? ("-" + request.getFacilityCode()) : "");
		awsRequest.setPolicyName(policyName.toString());
		awsRequest.setPolicyDocument(buildAWSUserPolicyDocumentForSQSQueue(sqsQueueArn));
		return awsRequest;
	}

	private String buildAWSUserPolicyDocumentForSQSQueue(String sqsQueueArn) throws HandlerException {

		if (sqsQueueArn == null || sqsQueueArn.equals("")) {
			LOG.error("buildAWSUserPolicyDocumentForSQSQueue() :: cannot be invoked without a valid SQS Queue ARN, throwing HandlerException");
			throw new HandlerException("buildAWSUserPolicyDocumentForSQSQueue() cannot be invoked without a valid SQS Queue ARN");
		}

		Map<String, ImmutableList<Object>> policy = ImmutableMap.of(
				"Statement",
				ImmutableList
						.builder()
						.add(ImmutableMap.builder()
								.put("Action", ImmutableList.of("sqs:DeleteMessage", "sqs:DeleteMessageBatch", "sqs:GetQueueAttributes", "sqs:GetQueueUrl", "sqs:ReceiveMessage"))
								.put("Effect", "Allow").put("Resource", sqsQueueArn).build()).build());

		try {
			return new ObjectMapper().writeValueAsString(policy);
		}
		catch (JsonProcessingException e) {
			LOG.error("buildAWSUserPolicyDocumentForSQSQueue() :: JsonProcessingExsception :: " + e);
			throw new HandlerException(e);
		}
	}

	private String createPrintServerConfigFile(ProvisionPrintServerRequest request, String accessKey, String secretKey) throws HandlerException {

		String path = "/tmp/" + request.getCompanyCode() + ".config.properties";
		Properties prop = new Properties();
		OutputStream output = null;

		try {

			output = new FileOutputStream(path);
			prop.setProperty("ACCESS_KEY", accessKey);
			prop.setProperty("SECRET_KEY", secretKey);
			prop.store(output, null);
		}
		catch (IOException io) {
			LOG.error("createPrintServerConfig() :: IOException :: " + io);
			throw new HandlerException(io);
		}
		finally {
			if (output != null) {
				try {
					output.close();
				}
				catch (IOException e) {
				}
			}

		}

		return path;

	}

}
