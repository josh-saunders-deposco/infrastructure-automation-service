package com.deposco.ias.handlers;

public class HandlerException extends Exception {
	private static final long serialVersionUID = 482186656453709592L;

	public HandlerException(Throwable t) {
		super(t);
	}

	public HandlerException(String message) {
		super(message);
	}

	public HandlerException(String message, Throwable t) {
		super(message, t);
	}
}
