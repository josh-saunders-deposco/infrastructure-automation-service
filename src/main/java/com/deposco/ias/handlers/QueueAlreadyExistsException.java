package com.deposco.ias.handlers;

public class QueueAlreadyExistsException extends HandlerException {

	private static final long serialVersionUID = 8855398325354723504L;

	public QueueAlreadyExistsException(String message) {
		super(message);
	}

}
