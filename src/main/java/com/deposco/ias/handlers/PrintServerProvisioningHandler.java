package com.deposco.ias.handlers;

import com.deposco.ias.model.ProvisionPrintServerRequest;
import com.deposco.ias.model.ProvisionPrintServerResponse;

public interface PrintServerProvisioningHandler {

	public ProvisionPrintServerResponse provisionPrinter(ProvisionPrintServerRequest request) throws HandlerException;

}
