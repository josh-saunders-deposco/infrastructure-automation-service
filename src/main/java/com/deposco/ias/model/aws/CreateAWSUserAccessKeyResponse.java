package com.deposco.ias.model.aws;

import com.amazonaws.services.identitymanagement.model.CreateAccessKeyResult;

public class CreateAWSUserAccessKeyResponse extends CreateAccessKeyResult {
	private static final long serialVersionUID = -3752978411852123054L;

	public CreateAWSUserAccessKeyResponse(CreateAccessKeyResult superObject) {
		this.setAccessKey(superObject.getAccessKey());
	}
}
