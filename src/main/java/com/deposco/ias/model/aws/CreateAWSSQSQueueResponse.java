package com.deposco.ias.model.aws;

import com.amazonaws.services.sqs.model.CreateQueueResult;

public class CreateAWSSQSQueueResponse extends CreateQueueResult {
	private static final long serialVersionUID = -661346532807136274L;

	public CreateAWSSQSQueueResponse(CreateQueueResult superObject) {
		this.setQueueUrl(superObject.getQueueUrl());
	}

}
