package com.deposco.ias.model.aws;

import com.amazonaws.services.identitymanagement.model.CreateUserRequest;

public class CreateAWSUserRequest extends CreateUserRequest {
	private static final long serialVersionUID = -8831305934712445089L;
}
