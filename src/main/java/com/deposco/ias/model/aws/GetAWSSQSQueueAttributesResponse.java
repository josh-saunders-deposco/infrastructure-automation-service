package com.deposco.ias.model.aws;

import com.amazonaws.services.sqs.model.GetQueueAttributesResult;

public class GetAWSSQSQueueAttributesResponse extends GetQueueAttributesResult {
	private static final long serialVersionUID = -494297669392372065L;

	public GetAWSSQSQueueAttributesResponse(GetQueueAttributesResult superObject) {
		this.setAttributes(superObject.getAttributes());
	}
}
