package com.deposco.ias.model.aws;

import com.amazonaws.services.identitymanagement.model.CreateUserResult;
import com.amazonaws.services.identitymanagement.model.User;

public class CreateAWSUserResponse extends CreateUserResult {
	private static final long serialVersionUID = -6334088445547992720L;

	public CreateAWSUserResponse(CreateUserResult superObject) {
		this.setUser(superObject.getUser());
	}

	public CreateAWSUserResponse(User user) {
		this.setUser(user);
	}
}
