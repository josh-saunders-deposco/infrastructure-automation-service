package com.deposco.ias.model.aws;

import com.amazonaws.services.sqs.model.CreateQueueRequest;

public class CreateAWSSQSQueueRequest extends CreateQueueRequest {

	private static final long serialVersionUID = 5812156297649399244L;
}
