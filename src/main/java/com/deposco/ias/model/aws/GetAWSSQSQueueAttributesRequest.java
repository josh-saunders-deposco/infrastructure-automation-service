package com.deposco.ias.model.aws;

import com.amazonaws.services.sqs.model.GetQueueAttributesRequest;

public class GetAWSSQSQueueAttributesRequest extends GetQueueAttributesRequest {

	private static final long serialVersionUID = -5690995607360136054L;

}
