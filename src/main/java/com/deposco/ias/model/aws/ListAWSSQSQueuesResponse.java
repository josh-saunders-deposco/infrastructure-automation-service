package com.deposco.ias.model.aws;

import com.amazonaws.services.sqs.model.ListQueuesResult;

public class ListAWSSQSQueuesResponse extends ListQueuesResult {
	private static final long serialVersionUID = -5599196131202651646L;

	public ListAWSSQSQueuesResponse(ListQueuesResult superObject) {
		this.setQueueUrls(superObject.getQueueUrls());
	}

}
