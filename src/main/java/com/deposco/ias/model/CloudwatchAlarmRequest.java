package com.deposco.ias.model;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class CloudwatchAlarmRequest implements Serializable {

	private static final long serialVersionUID = 2302011888164483876L;

	private String type;
	private String messageId;
	private String token;
	private String topicArn;
	private CloudwatchMessage message;
	private String subscribeURL;
	private String unsubscribeURL;
	private String timestamp;
	private String signatureVersion;
	private String signature;
	private String signingCertURL;
	private String subject;
	private String messageAttributes;

	public String getType() {
		return type;
	}

	@JsonProperty("Type")
	public void setType(String type) {
		this.type = type;
	}

	public String getMessageId() {
		return messageId;
	}

	@JsonProperty("MessageId")
	public void setMessageId(String messageId) {
		this.messageId = messageId;
	}

	public String getToken() {
		return token;
	}

	@JsonProperty("Token")
	public void setToken(String token) {
		this.token = token;
	}

	public String getTopicArn() {
		return topicArn;
	}

	@JsonProperty("TopicArn")
	public void setTopicArn(String topicArn) {
		this.topicArn = topicArn;
	}

	public CloudwatchMessage getMessage() {
		return message;
	}

	@JsonProperty("Message")
	public void setMessage(CloudwatchMessage message) {
		this.message = message;
	}

	public String getSubscribeURL() {
		return subscribeURL;
	}

	@JsonProperty("SubscribeURL")
	public void setSubscribeURL(String subscribeURL) {
		this.subscribeURL = subscribeURL;
	}

	public String getUnsubscribeURL() {
		return unsubscribeURL;
	}

	@JsonProperty("UnsubscribeURL")
	public void setUnsubscribeURL(String unsubscribeURL) {
		this.unsubscribeURL = unsubscribeURL;
	}

	public String getTimestamp() {
		return timestamp;
	}

	@JsonProperty("Timestamp")
	public void setTimestamp(String timestamp) {
		this.timestamp = timestamp;
	}

	public String getSignatureVersion() {
		return signatureVersion;
	}

	@JsonProperty("SignatureVersion")
	public void setSignatureVersion(String signatureVersion) {
		this.signatureVersion = signatureVersion;
	}

	public String getSignature() {
		return signature;
	}

	@JsonProperty("Signature")
	public void setSignature(String signature) {
		this.signature = signature;
	}

	public String getSigningCertURL() {
		return signingCertURL;
	}

	@JsonProperty("SigningCertURL")
	public void setSigningCertURL(String signingCertURL) {
		this.signingCertURL = signingCertURL;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	@JsonProperty("Subject")
	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getMessageAttributes() {
		return messageAttributes;
	}

	@JsonProperty("MessageAttributes")
	public void setMessageAttributes(String messageAttributes) {
		this.messageAttributes = messageAttributes;
	}

	@Override
	public String toString() {
		return "CloudwatchAlarmRequest [type=" + type + ", messageId=" + messageId + ", token=" + token + ", topicArn=" + topicArn + ", message=" + message + ", subscribeURL="
				+ subscribeURL + ", unsubscribeURL=" + unsubscribeURL + ", timestamp=" + timestamp + ", signatureVersion=" + signatureVersion + ", signature=" + signature
				+ ", signingCertURL=" + signingCertURL + ", subject=" + subject + ", messageAttributes=" + messageAttributes + "]";
	}
}
