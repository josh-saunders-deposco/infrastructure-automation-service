package com.deposco.ias.model;

import java.io.Serializable;
import java.util.Date;

public class BackupPrintServerRequest extends BasePrintServerRequest implements Serializable {

	private static final long serialVersionUID = 353070065814935428L;

	private Date timestamp;

	public Date getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(Date timestamp) {
		this.timestamp = timestamp;
	}

	@Override
	public String toString() {
		return "BackupPrintServerRequest [timestamp=" + timestamp + ", getCompanyCode()=" + getCompanyCode() + ", getFacilityCode()=" + getFacilityCode() + ", getServerCode()="
				+ getServerCode() + "]";
	}
}
