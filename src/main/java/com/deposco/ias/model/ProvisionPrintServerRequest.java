package com.deposco.ias.model;

import java.io.Serializable;

public class ProvisionPrintServerRequest extends BasePrintServerRequest implements Serializable {
	private static final long serialVersionUID = 3523380948566931652L;

	@Override
	public String toString() {
		return "ProvisionPrintServerRequest [getCompanyCode()=" + getCompanyCode() + ", getFacilityCode()=" + getFacilityCode() + ", getServerCode()=" + getServerCode() + "]";
	}
}
