package com.deposco.ias.model;

import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.ObjectMapper;

@JsonIgnoreProperties(ignoreUnknown = true)
public class CloudwatchMessage {

	private static final Logger LOG = LoggerFactory.getLogger(CloudwatchMessage.class);

	private String alarmName;
	private String alarmDescription;
	private String newStateValue;
	private String newStateReason;
	private String stateChangeTime;
	private String region;

	@JsonCreator
	public static CloudwatchMessage create(String json) {
		ObjectMapper mapper = new ObjectMapper();
		CloudwatchMessage message = null;

		try {
			message = mapper.readValue(json, CloudwatchMessage.class);
		}
		catch (IOException e) {
			LOG.error("create() :: exception while creating object :: " + e);
		}

		return message;
	}

	public String getAlarmName() {
		return alarmName;
	}

	@JsonProperty("AlarmName")
	public void setAlarmName(String alarmName) {
		this.alarmName = alarmName;
	}

	public String getAlarmDescription() {
		return alarmDescription;
	}

	@JsonProperty("AlarmDescription")
	public void setAlarmDescription(String alarmDescription) {
		this.alarmDescription = alarmDescription;
	}

	public String getNewStateValue() {
		return newStateValue;
	}

	@JsonProperty("NewStateValue")
	public void setNewStateValue(String newStateValue) {
		this.newStateValue = newStateValue;
	}

	public String getNewStateReason() {
		return newStateReason;
	}

	@JsonProperty("NewStateReason")
	public void setNewStateReason(String newStateReason) {
		this.newStateReason = newStateReason;
	}

	public String getStateChangeTime() {
		return stateChangeTime;
	}

	@JsonProperty("StateChangeTime")
	public void setStateChangeTime(String stateChangeTime) {
		this.stateChangeTime = stateChangeTime;
	}

	public String getRegion() {
		return region;
	}

	@JsonProperty("Region")
	public void setRegion(String region) {
		this.region = region;
	}

	@Override
	public String toString() {
		return "CloudwatchMessage [alarmName=" + alarmName + ", alarmDescription=" + alarmDescription + ", newStateValue=" + newStateValue + ", newStateReason=" + newStateReason
				+ ", stateChangeTime=" + stateChangeTime + ", region=" + region + "]";
	}
}
