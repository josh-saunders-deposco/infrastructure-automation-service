package com.deposco.ias.model;

public enum PrintServerStatus {
	CREATION_SUCCESSFUL, ALREADY_EXISTED
}
