package com.deposco.ias.model;

public class LogEntry {

	private String serverIp;
	private String timestamp;
	private String title;
	private String text;
	private String kibanaLink;

	public String getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(String timestamp) {
		this.timestamp = timestamp;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public String getServerIp() {
		return serverIp;
	}

	public void setServerIp(String serverIp) {
		this.serverIp = serverIp;
	}

	public String getKibanaLink() {
		return kibanaLink;
	}

	public void setKibanaLink(String kibanaLink) {
		this.kibanaLink = kibanaLink;
	}

	@Override
	public String toString() {
		return "LogEntry [serverIp=" + serverIp + ", timestamp=" + timestamp + ", title=" + title + ", text=" + text + ", kibanaLink=" + kibanaLink + "]";
	}
}
