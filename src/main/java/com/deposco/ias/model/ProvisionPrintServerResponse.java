package com.deposco.ias.model;

public class ProvisionPrintServerResponse {

	private String company;
	private String facility;
	private String queueName;
	private String accessKey;
	private String secretKey;
	private PrintServerStatus printServerStatus;

	public PrintServerStatus getPrintServerStatus() {
		return printServerStatus;
	}

	public void setPrintServerStatus(PrintServerStatus printServerStatus) {
		this.printServerStatus = printServerStatus;
	}

	public String getQueueName() {
		return queueName;
	}

	public void setQueueName(String queueName) {
		this.queueName = queueName;
	}

	public String getAccessKey() {
		return accessKey;
	}

	public void setAccessKey(String accessKey) {
		this.accessKey = accessKey;
	}

	public String getSecretKey() {
		return secretKey;
	}

	public void setSecretKey(String secretKey) {
		this.secretKey = secretKey;
	}

	public String getCompany() {
		return company;
	}

	public void setCompany(String company) {
		this.company = company;
	}

	public String getFacility() {
		return facility;
	}

	public void setFacility(String facility) {
		this.facility = facility;
	}
}
