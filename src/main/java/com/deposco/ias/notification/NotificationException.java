package com.deposco.ias.notification;

public class NotificationException extends Exception {

	private static final long serialVersionUID = -8824153423262157890L;

	public NotificationException() {
	}

	public NotificationException(String message) {
		super(message);
	}

	public NotificationException(String message, Throwable t) {
		super(message, t);
	}

	public NotificationException(Throwable t) {
		super(t);
	}

}
