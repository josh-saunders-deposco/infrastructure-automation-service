package com.deposco.ias.notification;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Properties;
import java.util.TimeZone;

import org.springframework.stereotype.Component;

import com.deposco.ias.notification.model.SlackNotification;

import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateExceptionHandler;
import freemarker.template.Version;

@Component
public class SlackNotificationFactory {

	private static Configuration cfg;
	private static Properties properties = null;

	static {
		cfg = new Configuration();
		try {
			cfg.setDirectoryForTemplateLoading(new File("/etc/deposco/notifications/templates"));
		}
		catch (IOException e) {
		}
		// cfg.setClassForTemplateLoading(HTMLEmailFactory.class, "templates");
		cfg.setIncompatibleImprovements(new Version(2, 3, 20));
		cfg.setDefaultEncoding("UTF-8");
		cfg.setLocale(Locale.US);
		cfg.setTemplateExceptionHandler(TemplateExceptionHandler.RETHROW_HANDLER);
	}

	public SlackNotification create(String notificationType, String timestamp) throws NotificationException {
		SlackNotification notification = null;
		try {
			Template template = cfg.getTemplate("slack.ftl");
			String subject = getProperties().getProperty(notificationType + ".subject");
			Map<String, Object> params = new HashMap<String, Object>();
			params.put("title", getProperties().getProperty(notificationType + ".subject"));

			String timeWindowInMinutes = getProperties().getProperty(notificationType + ".logTimeWindowInMinutes");
			String queryPhrase = getProperties().getProperty(notificationType + ".logQueryPhrase");
			String logLevel = getProperties().getProperty(notificationType + ".logLevel");
			String logEnvironmentsString = getProperties().getProperty(notificationType + ".logEnvironments");
			String logType = getProperties().getProperty(notificationType + ".logType");
			String[] logEnvironments = null;
			if (logEnvironmentsString != null && logEnvironmentsString.length() > 0) logEnvironments = logEnvironmentsString.split(",");

			TimeZone timezone = TimeZone.getTimeZone("America/New_York");
			Calendar from = Calendar.getInstance(timezone);
			from.add(Calendar.MINUTE, -Integer.parseInt(timeWindowInMinutes));
			Calendar to = Calendar.getInstance(timezone);
			SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ");
			format.setTimeZone(timezone);
			String fromString = format.format(from.getTime());
			String toString = format.format(to.getTime());

			params.put("kibanalinkurl", "http://logs.deposco.com/_plugin/kibana3/#/dashboard/elasticsearch/Cloudwatch-Template?to=" + toString + "&from=" + fromString + "&level="
					+ logLevel + "&env=" + buildEnvironmentQuery(logEnvironments) + "&query=" + ("\\\"" + queryPhrase.replaceAll("\"", "\\\\\"") + "\\\"").replaceAll(" ", "%20"));

			SimpleDateFormat timeFormat = new SimpleDateFormat("hh:mm a");
			timeFormat.setTimeZone(timezone);
			params.put("timestamp", timeFormat.format(to.getTime()));

			StringWriter writer = new StringWriter();
			template.process(params, writer);
			String slackBody = writer.toString();
			notification = new SlackNotification();
			notification.setBody(slackBody);
		}
		catch (Throwable e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return notification;
	}

	private String buildEnvironmentQuery(String[] environments) {
		if (environments == null || environments.length < 1) return null;

		StringBuilder buffer = new StringBuilder();

		for (String environment : environments) {
			buffer.append("\\\"" + environment + "\\\"" + " OR ");
		}

		buffer.delete(buffer.length() - 4, buffer.length());

		return buffer.toString().replaceAll(" ", "%20");
	}

	private static Properties getProperties() {
		if (properties != null) return properties;

		try {
			InputStream stream = new FileInputStream("/etc/deposco/notifications/notificationtemplatemapping.properties");
			properties = new Properties();
			properties.load(stream);
			stream.close();
		}
		catch (Exception e) {
		}
		return properties;
	}
}
