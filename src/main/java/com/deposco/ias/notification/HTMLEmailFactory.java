package com.deposco.ias.notification;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Properties;
import java.util.TimeZone;

import net.logstash.logback.encoder.org.apache.commons.lang.StringEscapeUtils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.deposco.ias.brokers.LogQueryBroker;
import com.deposco.ias.brokers.LogQueryException;
import com.deposco.ias.model.LogEntry;
import com.deposco.ias.notification.model.Email;
import com.deposco.ias.notification.model.HTMLEmail;

import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import freemarker.template.TemplateExceptionHandler;
import freemarker.template.Version;

@Component
public class HTMLEmailFactory implements EmailFactory {

	private static final Logger LOG = LoggerFactory.getLogger(HTMLEmailFactory.class);
	private static Configuration cfg;
	private static Properties properties = null;

	@Autowired
	private LogQueryBroker logQueryBroker;

	static {
		cfg = new Configuration();
		try {
			cfg.setDirectoryForTemplateLoading(new File("/etc/deposco/notifications/templates"));
		}
		catch (IOException e) {
			LOG.error("Exception while setting template directory :: " + e, e);
		}
		// cfg.setClassForTemplateLoading(HTMLEmailFactory.class, "templates");
		cfg.setIncompatibleImprovements(new Version(2, 3, 20));
		cfg.setDefaultEncoding("UTF-8");
		cfg.setLocale(Locale.US);
		cfg.setTemplateExceptionHandler(TemplateExceptionHandler.RETHROW_HANDLER);
	}

	@SuppressWarnings("unchecked")
	@Override
	public Email create(String notificationType, String timestampString) throws NotificationException {
		LOG.info("create() :: called with notification type :: " + notificationType + " :: timestamp :: " + timestampString);
		Email response = null;

		try {

			if (shouldBeSuppressed(notificationType)) throw new SuppressNotificationException();

			Template template = cfg.getTemplate(getProperties().getProperty(notificationType + ".template"));
			String subject = getProperties().getProperty(notificationType + ".subject");

			Map<String, Object> params = new HashMap<String, Object>();

			params.put("title", getProperties().getProperty(notificationType + ".subject"));
			params.put("description", getProperties().getProperty(notificationType + ".description"));

			String timeWindowInMinutes = getProperties().getProperty(notificationType + ".logTimeWindowInMinutes");
			String queryPhrase = getProperties().getProperty(notificationType + ".logQueryPhrase");
			String logLevel = getProperties().getProperty(notificationType + ".logLevel");
			String logEnvironmentsString = getProperties().getProperty(notificationType + ".logEnvironments");
			String logType = getProperties().getProperty(notificationType + ".logType");
			String[] logEnvironments = null;
			if (logEnvironmentsString != null && logEnvironmentsString.length() > 0) logEnvironments = logEnvironmentsString.split(",");
			String numberOfLogEntries = getProperties().getProperty(notificationType + ".numberOfLogEntries");

			TimeZone timezone = TimeZone.getTimeZone("America/New_York");
			Calendar from = Calendar.getInstance(timezone);
			from.add(Calendar.MINUTE, -Integer.parseInt(timeWindowInMinutes));
			Calendar to = Calendar.getInstance(timezone);
			SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ");
			format.setTimeZone(timezone);
			String fromString = format.format(from.getTime());
			String toString = format.format(to.getTime());

			params.put("fromString", fromString);
			params.put("toString", toString);

			if ("true".equalsIgnoreCase(getProperties().getProperty(notificationType + ".includeKibanaLink"))) {
				params.put("kibanalinkdescription", "View these incedents in ");
				params.put(
						"kibanalinkurl",
						"http://logs.deposco.com/_plugin/kibana3/#/dashboard/elasticsearch/Cloudwatch-Template?to=" + toString + "&from=" + fromString + "&level=" + logLevel
								+ "&env=" + StringEscapeUtils.escapeHtml(buildEnvironmentQuery(logEnvironments)) + "&query="
								+ StringEscapeUtils.escapeHtml("\\\"" + queryPhrase.replaceAll("\"", "\\\\\"") + "\\\""));
			}

			if ("true".equalsIgnoreCase(getProperties().getProperty(notificationType + ".includeLogs"))) {
				List<LogEntry> logEntries = logQueryBroker.queryLogs(convertToDate(timestampString), Integer.parseInt(timeWindowInMinutes), queryPhrase, logLevel, logEnvironments,
						Integer.parseInt(numberOfLogEntries), logType);
				params.put("logEntries", logEntries);
			}

			String fromAddress = getProperties().getProperty(notificationType + ".fromAddress");
			List<String> toAddresses = splitCSV(getProperties().getProperty(notificationType + ".toAddresses"));

			if ("true".equalsIgnoreCase(getProperties().getProperty(notificationType + ".includeLogs"))
					&& (params.get("logEntries") == null || ((List<LogEntry>) params.get("logEntries")).size() < 1)) {
				toAddresses = new ArrayList<String>();
				toAddresses.add("jsaunders@deposco.com");
			}

			StringWriter writer = new StringWriter();
			template.process(params, writer);
			String html = writer.toString();
			writer.close();
			LOG.info("create() :: subject :: " + subject);
			LOG.info("create() :: fromAddress :: " + fromAddress);
			LOG.info("create() :: toAddresses :: " + toAddresses);
			LOG.info("create() :: email body :: " + html);
			response = new HTMLEmail();
			response.setBody(html);
			response.setFromAddress(fromAddress);
			response.setSubject(subject);
			response.setToAddresses(toAddresses);
		}
		catch (TemplateException | NumberFormatException | LogQueryException | ParseException | IOException e) {
			LOG.error("create() :: Exception while creating email :: " + e, e);
			throw new NotificationException(e);
		}

		return response;
	}

	private String buildEnvironmentQuery(String[] environments) {
		if (environments == null || environments.length < 1) return null;

		StringBuilder buffer = new StringBuilder();

		for (String environment : environments) {
			buffer.append("\\\"" + environment + "\\\"" + " OR ");
		}

		buffer.delete(buffer.length() - 4, buffer.length());

		return buffer.toString();
	}

	private List<String> splitCSV(String csv) {
		return Arrays.asList(csv.split(","));
	}

	private boolean shouldBeSuppressed(String notificationType) {

		if (getProperties().containsKey(notificationType + ".suppressionPeriods")) {
			String suppressionPeriodsString = getProperties().getProperty(notificationType + ".suppressionPeriods");
			String[] suppressionPeriods = suppressionPeriodsString.split(",");
			if (suppressionPeriods != null && suppressionPeriods.length > 0) {
				for (String suppressionPeriod : suppressionPeriods) {
					if (suppressionPeriod != null) {
						String[] tokens = suppressionPeriod.split("-");

						if (tokens != null && tokens.length == 2) {
							String startTime = tokens[0];
							String endTime = tokens[1];

							if (startTime != null && endTime != null) {
								String[] startTimeTokens = startTime.split(":");
								String[] endTimeTokens = endTime.split(":");
								if (startTimeTokens != null && startTimeTokens.length == 2 && endTimeTokens != null && endTimeTokens.length == 2) {
									int startTimeHour = Integer.parseInt(startTimeTokens[0]);
									int startTimeMinutes = Integer.parseInt(startTimeTokens[1]);
									int endTimeHour = Integer.parseInt(endTimeTokens[0]);
									int endTimeMinutes = Integer.parseInt(endTimeTokens[1]);
									Calendar now = Calendar.getInstance(TimeZone.getTimeZone("America/New_York"));

									Calendar start = Calendar.getInstance(TimeZone.getTimeZone("America/New_York"));
									start.set(Calendar.HOUR_OF_DAY, startTimeHour);
									start.set(Calendar.MINUTE, startTimeMinutes);

									Calendar end = Calendar.getInstance(TimeZone.getTimeZone("America/New_York"));
									end.set(Calendar.HOUR_OF_DAY, endTimeHour);
									end.set(Calendar.MINUTE, endTimeMinutes);

									if (now.after(start) && now.before(end)) {
										LOG.info("shouldBeSuppressed() :: now is between " + startTime + " and " + endTime + " :: suppressing notification");
										return true;
									}
								}
							}
						}
					}
				}
			}

		}

		if (getProperties().containsKey(notificationType + ".disabled")) {

			if ("true".equalsIgnoreCase(getProperties().getProperty(notificationType + ".disabled"))) return true;
		}

		return false;
	}

	private Date convertToDate(String timestamp) throws ParseException {
		DateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'hh:mm:ss");
		Date date = format.parse(timestamp);
		return date;
	}

	private static Properties getProperties() {
		if (properties != null) return properties;

		try {
			InputStream stream = new FileInputStream("/etc/deposco/notifications/notificationtemplatemapping.properties");
			properties = new Properties();
			properties.load(stream);
			stream.close();
			LOG.info("getProperties() :: loaded properties :: " + properties);
		}
		catch (Exception e) {
			LOG.error("getProperties() :: Exception whiule loading properties :: " + e, e);
		}
		return properties;
	}
}
