package com.deposco.ias.notification;

import com.deposco.ias.notification.model.Email;

public interface EmailFactory {
	public Email create(String notificationType, String timestamp) throws NotificationException;

}
