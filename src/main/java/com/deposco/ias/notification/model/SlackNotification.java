package com.deposco.ias.notification.model;

public class SlackNotification {
	private String body;

	public String getBody() {
		return body;
	}

	public void setBody(String body) {
		this.body = body;
	}
}
