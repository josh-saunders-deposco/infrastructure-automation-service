package com.deposco.ias.notification.model;

import java.util.List;

public class Email {
	private String subject;
	private String body;
	private List<String> toAddresses;
	private String fromAddress;

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getBody() {
		return body;
	}

	public void setBody(String body) {
		this.body = body;
	}

	public List<String> getToAddresses() {
		return toAddresses;
	}

	public void setToAddresses(List<String> toAddresses) {
		this.toAddresses = toAddresses;
	}

	public String getFromAddress() {
		return fromAddress;
	}

	public void setFromAddress(String fromAddress) {
		this.fromAddress = fromAddress;
	}

	@Override
	public String toString() {
		return "Email [subject=" + subject + ", body=" + body + ", toAddresses=" + toAddresses + ", fromAddress=" + fromAddress + "]";
	}
}
