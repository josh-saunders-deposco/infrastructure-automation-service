package com.deposco.ias.db.model;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "ALARM", catalog = "INFRASTRUCTURE_AUTOMATION")
public class Alarm implements Serializable {

	private static final long serialVersionUID = -7007612080126405401L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID", unique = true, nullable = false)
	private int id;

	@Column(name = "NAME", length = 50)
	private String name;

	@Column(name = "QUERY", length = 500)
	private String query;

	@Column(name = "LOG_LEVEL", length = 20)
	private String logLevel;

	@OneToMany(mappedBy = "ALARM", cascade = { CascadeType.ALL })
	@JoinColumn(name = "alarm_id")
	private Set<Notification> notifications;

	@OneToMany(mappedBy = "ALARM", cascade = { CascadeType.ALL })
	@JoinColumn(name = "alarm_id")
	private Set<Environment> environments;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Set<Notification> getNotifications() {
		return notifications;
	}

	public void setNotifications(Set<Notification> notifications) {
		this.notifications = notifications;
	}

	public String getQuery() {
		return query;
	}

	public void setQuery(String query) {
		this.query = query;
	}

	public String getLogLevel() {
		return logLevel;
	}

	public void setLogLevel(String logLevel) {
		this.logLevel = logLevel;
	}

	public Set<Environment> getEnvironments() {
		return environments;
	}

	public void setEnvironments(Set<Environment> environments) {
		this.environments = environments;
	}

	@Override
	public String toString() {
		return "Alarm [name=" + name + ", notifications=" + notifications + ", query=" + query + ", logLevel=" + logLevel + ", environments=" + environments + "]";
	}
}
