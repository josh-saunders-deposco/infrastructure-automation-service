package com.deposco.ias.db.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "NOTIFICATION")
public class Notification implements Serializable {

	private static final long serialVersionUID = 3247224968930138144L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID", unique = true, nullable = false)
	private int id;

	@Column(name = "SUBJECT", length = 150)
	private String subject;

	@Column(name = "DESCRIPTION", length = 150)
	private String description;

	@Column(name = "FROM_ADDRESS", length = 50)
	private String fromAddress;

	@OneToMany(mappedBy = "NOTIFICATION", cascade = { CascadeType.ALL })
	@JoinColumn(name = "notification_id")
	private List<String> toAddresses;

	@Column(name = "TEMPLATE_NAME", length = 50)
	private String templateName;

	@Column(name = "SUPPRESSED")
	private boolean suppressed;

	@OneToMany(mappedBy = "NOTIFICATION", cascade = { CascadeType.ALL })
	@JoinColumn(name = "notification_id")
	private List<String> quietPeriods;

	public boolean isSuppressed() {
		return suppressed;
	}

	public void setSuppressed(boolean suppressed) {
		this.suppressed = suppressed;
	}

	public List<String> getQuietPeriods() {
		return quietPeriods;
	}

	public void setQuietPeriods(List<String> quietPeriods) {
		this.quietPeriods = quietPeriods;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getFromAddress() {
		return fromAddress;
	}

	public void setFromAddress(String fromAddress) {
		this.fromAddress = fromAddress;
	}

	public List<String> getToAddresses() {
		return toAddresses;
	}

	public void setToAddresses(List<String> toAddresses) {
		this.toAddresses = toAddresses;
	}

	public String getTemplateName() {
		return templateName;
	}

	public void setTemplateName(String templateName) {
		this.templateName = templateName;
	}

	@Override
	public String toString() {
		return "Notification [subject=" + subject + ", description=" + description + ", fromAddress=" + fromAddress + ", toAddresses=" + toAddresses + ", templateName="
				+ templateName + ", suppressed=" + suppressed + ", quietPeriods=" + quietPeriods + "]";
	}
}
