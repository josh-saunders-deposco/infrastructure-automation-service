package com.deposco.ias.db.model;

import java.io.Serializable;

public enum Environment implements Serializable {

	f01, f02, f03, el, printserver_prod {
		public String toString() {
			return "printserver-prod";
		}
	}

}
