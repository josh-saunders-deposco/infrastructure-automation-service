package com.deposco.ias.services;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.deposco.ias.handlers.CloudwatchAlarmHandler;
import com.deposco.ias.handlers.HandlerException;
import com.deposco.ias.model.CloudwatchAlarmRequest;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@Component
@Path("/cloudwatch")
@Consumes({ MediaType.APPLICATION_JSON, MediaType.TEXT_PLAIN })
public class CloudwatchAlarmService {

	private static final Logger LOG = LoggerFactory.getLogger(CloudwatchAlarmService.class);

	@Autowired
	private CloudwatchAlarmHandler handler;

	@POST
	@Path("/alarm")
	@Consumes({ MediaType.TEXT_PLAIN })
	public Response alertText(String json) {

		LOG.info("alertText() called with raw json :: " + json);

		try {
			ObjectMapper mapper = new ObjectMapper();
			CloudwatchAlarmRequest request = mapper.readValue(json, CloudwatchAlarmRequest.class);
			return alertJson(request);
		}
		catch (JsonParseException e) {
			LOG.error("alertText() :: invalid json format :: " + e);
			throw new WebApplicationException(Response.Status.BAD_REQUEST);
		}
		catch (JsonMappingException e) {
			LOG.error("alertText() :: invalid json format :: " + e);
			throw new WebApplicationException(Response.Status.BAD_REQUEST);
		}
		catch (Throwable t) {
			LOG.error("alertText() :: Generic Throwable occurred :: " + t);
			throw new WebApplicationException(Response.Status.INTERNAL_SERVER_ERROR);
		}
	}

	@POST
	@Path("/alarm")
	@Consumes({ MediaType.APPLICATION_JSON })
	public Response alertJson(CloudwatchAlarmRequest request) {
		LOG.info("alertJson() called with request :: " + request);

		try {
			handler.handleAlarm(request);
		}
		catch (HandlerException e) {
			LOG.error("alertJson() :: Handler Exception occurred :: " + e);
			throw new WebApplicationException(Response.Status.INTERNAL_SERVER_ERROR);
		}
		catch (Throwable t) {
			LOG.error("alertJson() :: Generic Throwable occurred :: " + t);
			throw new WebApplicationException(Response.Status.INTERNAL_SERVER_ERROR);
		}

		LOG.info("alertJson() :: success");
		return Response.ok().build();
	}
}
