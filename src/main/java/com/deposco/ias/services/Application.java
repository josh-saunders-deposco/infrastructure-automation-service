package com.deposco.ias.services;

import org.glassfish.jersey.jackson.JacksonFeature;
import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.server.spring.scope.RequestContextFilter;

public class Application extends ResourceConfig {

	public Application() {
		register(RequestContextFilter.class);
		register(JacksonFeature.class);
		register(PrintServerProvisioningService.class);
		register(CloudwatchAlarmService.class);
	}
}
