package com.deposco.ias.services;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.deposco.ias.handlers.HandlerException;
import com.deposco.ias.handlers.PrintServerProvisioningHandler;
import com.deposco.ias.model.ProvisionPrintServerRequest;
import com.deposco.ias.model.ProvisionPrintServerResponse;

@Component
@Path("/printserver")
@Produces({ MediaType.APPLICATION_JSON })
@Consumes({ MediaType.APPLICATION_JSON })
public class PrintServerProvisioningService {
	private static final Logger LOG = LoggerFactory.getLogger(PrintServerProvisioningService.class);

	@Autowired
	private PrintServerProvisioningHandler handler;

	@POST
	@Path("/provision")
	@Consumes({ MediaType.APPLICATION_JSON })
	@Produces({ MediaType.APPLICATION_JSON })
	public ProvisionPrintServerResponse provisionPrintServer(ProvisionPrintServerRequest request) {
		LOG.info("provisionPrintServer() called with request :: " + request);

		ProvisionPrintServerResponse response = null;
		try {
			response = handler.provisionPrinter(request);
		}
		catch (HandlerException e) {
			LOG.error("provisionPrintServer() :: Handler Exception occurred :: " + e);
			throw new WebApplicationException(Response.Status.INTERNAL_SERVER_ERROR);
		}
		catch (Throwable t) {
			LOG.error("provisionPrintServer() :: Generic Throwable occurred :: " + t);
			throw new WebApplicationException(Response.Status.INTERNAL_SERVER_ERROR);
		}

		LOG.info("provisionPrintServer() :: success");
		return response;
	}

	// @POST
	// @Path("/backup")
	// @Consumes({ MediaType.MULTIPART_FORM_DATA })
	// @Produces({ MediaType.APPLICATION_JSON })
	// public Response backupPrintServer(@FormDataParam("request") BackupPrintServerRequest request, @FormDataParam("file") InputStream backupFile,
	// @FormDataParam("file") FormDataContentDisposition backupFileDetail) {
	// LOG.info("backupPrintServer() called");
	// // TODO code
	// LOG.info("backupPrintServer() :: success");
	// return Response.ok().build();
	// }
}
