package com.deposco.ias.brokers;

public interface PrinterBackupBroker {

	public void createBackupDirectoryIfNeeded(String companyCode, String queueName);

}
