package com.deposco.ias.brokers.impl;

import io.searchbox.client.JestClient;
import io.searchbox.client.JestClientFactory;
import io.searchbox.client.config.HttpClientConfig;
import io.searchbox.core.Search;
import io.searchbox.core.SearchResult;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import net.logstash.logback.encoder.org.apache.commons.lang.StringEscapeUtils;

import org.elasticsearch.index.query.FilterBuilders;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.elasticsearch.search.sort.SortOrder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.deposco.ias.brokers.LogQueryBroker;
import com.deposco.ias.brokers.LogQueryException;
import com.deposco.ias.model.LogEntry;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

@Component
public class LogQueryBrokerImpl implements LogQueryBroker {
	private static final Logger LOG = LoggerFactory.getLogger(LogQueryBrokerImpl.class);

	private static JestClient client = null;

	public LogQueryBrokerImpl() {
		JestClientFactory factory = new JestClientFactory();
		factory.setHttpClientConfig(new HttpClientConfig.Builder("http://logs.deposco.com").multiThreaded(true).build());
		client = factory.getObject();
	}

	public List<LogEntry> queryLogs(Date timestamp, int timeWindowInMinutes, String queryPhrase, String logLevel, String[] logEnvironments, int numberOfLogEntries, String logType)
			throws LogQueryException {
		List<LogEntry> logEntries = new ArrayList<LogEntry>();

		Calendar now = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
		Calendar then = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
		then.add(Calendar.MINUTE, -timeWindowInMinutes);

		SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
		searchSourceBuilder
				.query(QueryBuilders.filteredQuery(
						QueryBuilders.matchQuery("log_level", logLevel),
						FilterBuilders.andFilter(FilterBuilders.rangeFilter("@timestamp").timeZone("UTC").lte(now.getTime()).gte(then.getTime()),
								FilterBuilders.queryFilter(QueryBuilders.matchPhraseQuery("@message", queryPhrase.toLowerCase())),
								FilterBuilders.queryFilter(QueryBuilders.termsQuery("env", logEnvironments).minimumMatch(1))))).sort("@timestamp", SortOrder.DESC).from(0)
				.size(numberOfLogEntries);

		Search search = new Search.Builder(searchSourceBuilder.toString()).addIndex(getIndexName()).addType(logType).build();
		LOG.info("queryLogs() :: " + search.toString());

		try {
			SearchResult result = client.execute(search);
			JsonObject jsonObject = result.getJsonObject();
			JsonArray hits = jsonObject.getAsJsonObject("hits").getAsJsonArray("hits");

			TimeZone timezone = TimeZone.getTimeZone("America/New_York");
			Calendar from = Calendar.getInstance(timezone);
			from.add(Calendar.MINUTE, -timeWindowInMinutes);
			Calendar to = Calendar.getInstance(timezone);
			SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ");
			format.setTimeZone(timezone);
			String fromString = format.format(from.getTime());
			String toString = format.format(to.getTime());

			for (int i = 0; i < hits.size(); i++) {

				JsonObject hit = hits.get(i).getAsJsonObject().get("_source").getAsJsonObject();
				LogEntry entry = new LogEntry();
				entry.setTimestamp(hit.get("timestamp").getAsString());
				entry.setServerIp(hit.get("@log_stream").getAsString());
				String message = hit.get("@message").getAsString();

				if (message != null && message.contains("PrintServerTask") && message.contains("jstest2")) continue;

				if (message != null && message.length() > 200) {
					String[] tokens = message.split("\n");
					if (tokens[0] != null && tokens[0].length() > 5) {
						entry.setTitle(tokens[0]);
					}
					else {
						entry.setTitle(message.substring(0, 200) + "...");
					}

				}
				else {
					entry.setTitle(message);
				}

				entry.setText(message.replaceAll("(\r\n|\n\r|\r|\n)", "<br />"));

				String pid = hit.get("pid") != null ? hit.get("pid").getAsString() : "";

				entry.setKibanaLink("http://logs.deposco.com/_plugin/kibana3/#/dashboard/elasticsearch/Cloudwatch-Transaction-Template?to=" + toString + "&from=" + fromString
						+ "&env=" + StringEscapeUtils.escapeHtml(buildEnvironmentQuery(logEnvironments)) + "&query=" + StringEscapeUtils.escapeHtml("\\\"" + pid + "\\\""));

				LOG.info("queryLogs() :: hit :: " + entry);
				logEntries.add(entry);
			}
		}
		catch (Throwable t) {
			LOG.error("queryLogs() :: Exception :: " + t, t);
			throw new LogQueryException(t);
		}

		return logEntries;
	}

	private String buildEnvironmentQuery(String[] environments) {
		if (environments == null || environments.length < 1) return null;

		StringBuilder buffer = new StringBuilder();

		for (String environment : environments) {
			buffer.append("\\\"" + environment + "\\\"" + " OR ");
		}

		buffer.delete(buffer.length() - 4, buffer.length());

		return buffer.toString();
	}

	private String getIndexName() {
		Date now = new Date();
		DateFormat format = new SimpleDateFormat("yyyy.MM.dd");
		return "cwl-" + format.format(now);
	}

}
