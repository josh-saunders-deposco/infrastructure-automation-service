package com.deposco.ias.brokers.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.deposco.ias.brokers.PrinterBackupBroker;
import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.Session;
import com.jcraft.jsch.SftpException;

@Component
public class LegacyPrinterBackupBrokerImpl implements PrinterBackupBroker {

	private static final Logger LOG = LoggerFactory.getLogger(LegacyPrinterBackupBrokerImpl.class);

	private static final String USER = "deposco";
	private static final String PASSWORD = "";
	private static final String HOST = "init.deposco.com";
	private static final int PORT = 22;
	private static final String BASE_DIR = "/home/deposco/ppd/";
	private static final String RSA_PATH = "/usr/share/tomcat8/conf/keys/dev";

	@Override
	public void createBackupDirectoryIfNeeded(String companyCode, String queueName) {
		Session session = null;
		Channel channel = null;

		try {
			JSch jsch = new JSch();
			jsch.addIdentity(RSA_PATH, PASSWORD);
			session = jsch.getSession(USER, HOST, PORT);
			java.util.Properties config = new java.util.Properties();
			config.put("StrictHostKeyChecking", "no");
			session.setConfig(config);
			session.connect();
			channel = session.openChannel("sftp");
			channel.connect();
			ChannelSftp sftpChannel = (ChannelSftp) channel;

			try {

				sftpChannel.cd(BASE_DIR);

				try {
					sftpChannel.mkdir("./" + companyCode);
				}
				catch (SftpException e) {
					LOG.info("createBackupDirectoryIfNeeded() :: Exception while creating the parent directory :: it probably already existed :: " + e);
				}

				sftpChannel.mkdir("./" + companyCode + "/" + queueName);
			}
			catch (SftpException e) {
				LOG.info("createBackupDirectoryIfNeeded() :: Exception while creating directory :: it probably already existed :: " + e);
			}
		}
		catch (Throwable t) {
			LOG.error("createBackupDirectoryIfNeeded() :: Exception :: " + t);
		}
		finally {
			try {
				if (channel != null && channel.isConnected()) channel.disconnect();
				if (session != null && session.isConnected()) session.disconnect();
			}
			catch (Throwable t) {
			}
		}
	}

}
