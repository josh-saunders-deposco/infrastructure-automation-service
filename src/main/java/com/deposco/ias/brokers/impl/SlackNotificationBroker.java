package com.deposco.ias.brokers.impl;

import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.springframework.stereotype.Component;

import com.deposco.ias.notification.model.SlackNotification;

@Component
public class SlackNotificationBroker {

	private static String SLACK_URL = "https://deposco.slack.com/services/hooks/slackbot?token=7IQ3byDoyaUhseXRrRStSyrK&channel=%23production-alerts";

	public void sendSlackNotification(SlackNotification slackNotification) {

		StringEntity body;
		try {
			body = new StringEntity(slackNotification.getBody());
			DefaultHttpClient httpClient = new DefaultHttpClient();
			HttpPost httpRequest = new HttpPost(SLACK_URL);
			// httpRequest.setHeader("Content-Type", "text/plain");
			httpRequest.setEntity(body);
			HttpResponse httpresponse = httpClient.execute(httpRequest);
			String result = EntityUtils.toString(httpresponse.getEntity());
			System.out.println(result);
		}
		catch (Throwable e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public static void main(String[] args) {
		SlackNotificationBroker slackNotificationBroker = new SlackNotificationBroker();
		SlackNotification slackNotification = new SlackNotification();
		slackNotification.setBody("Hello from the Deposco Infrastructure Automation Service!!!");
		slackNotificationBroker.sendSlackNotification(slackNotification);
	}

}
