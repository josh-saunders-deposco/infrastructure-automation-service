package com.deposco.ias.brokers;

public class LogQueryException extends Exception {

	private static final long serialVersionUID = -6086549837142887927L;

	public LogQueryException(Throwable t) {
		super(t);
	}

	public LogQueryException(String message, Throwable t) {
		super(message, t);
	}

	public LogQueryException(String message) {
		super(message);
	}
}
