package com.deposco.ias.brokers.aws;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.ClasspathPropertiesFileCredentialsProvider;

public class AWSCredentialsFactory {

	private static ClasspathPropertiesFileCredentialsProvider credentialsProvider = new ClasspathPropertiesFileCredentialsProvider();

	public static AWSCredentials loadCredentials() {
		return credentialsProvider.getCredentials();
	}

}
