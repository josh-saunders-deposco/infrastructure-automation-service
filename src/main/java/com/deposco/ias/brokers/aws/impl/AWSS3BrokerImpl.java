package com.deposco.ias.brokers.aws.impl;

import java.io.File;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.AmazonS3Exception;
import com.amazonaws.services.s3.model.CannedAccessControlList;
import com.amazonaws.services.s3.model.GetObjectRequest;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.deposco.ias.brokers.aws.AWSBaseBroker;
import com.deposco.ias.brokers.aws.AWSException;
import com.deposco.ias.brokers.aws.AWSS3Broker;

@Component
public class AWSS3BrokerImpl extends AWSBaseBroker implements AWSS3Broker {
	private static final Logger LOG = LoggerFactory.getLogger(AWSS3BrokerImpl.class);

	@Override
	public void createFileAtPath(String bucketName, File file, String s3FilePath, boolean isPublic) throws AWSException {
		AmazonS3 client = new AmazonS3Client(awsCredentials);
		client.putObject(new PutObjectRequest(bucketName, s3FilePath, file).withCannedAcl(isPublic ? CannedAccessControlList.PublicRead : CannedAccessControlList.Private));
	}

	@Override
	public void populateFileUsingFileAtPath(String bucketName, String s3FilePath, File dest) throws AWSException {
		try {
			AmazonS3 client = new AmazonS3Client(awsCredentials);
			client.getObject(new GetObjectRequest(bucketName, s3FilePath), dest);
			LOG.info("populateFileUsingFileAtPath() :: file found");
		}
		catch (AmazonS3Exception e) {
			LOG.info("populateFileUsingFileAtPath() :: no file found :: " + e);
		}
	}
}
