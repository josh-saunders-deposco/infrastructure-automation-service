package com.deposco.ias.brokers.aws;

import com.amazonaws.auth.AWSCredentials;

public abstract class AWSBaseBroker {
	protected static AWSCredentials awsCredentials = AWSCredentialsFactory.loadCredentials();
}
