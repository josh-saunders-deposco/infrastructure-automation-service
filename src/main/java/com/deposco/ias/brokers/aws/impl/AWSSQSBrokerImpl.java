package com.deposco.ias.brokers.aws.impl;

import org.springframework.stereotype.Component;

import com.amazonaws.services.sqs.AmazonSQSClient;
import com.deposco.ias.brokers.aws.AWSBaseBroker;
import com.deposco.ias.brokers.aws.AWSException;
import com.deposco.ias.brokers.aws.AWSSQSBroker;
import com.deposco.ias.model.aws.CreateAWSSQSQueueRequest;
import com.deposco.ias.model.aws.CreateAWSSQSQueueResponse;
import com.deposco.ias.model.aws.GetAWSSQSQueueAttributesRequest;
import com.deposco.ias.model.aws.GetAWSSQSQueueAttributesResponse;
import com.deposco.ias.model.aws.ListAWSSQSQueuesResponse;

@Component
public class AWSSQSBrokerImpl extends AWSBaseBroker implements AWSSQSBroker {

	@Override
	public CreateAWSSQSQueueResponse createQueue(CreateAWSSQSQueueRequest createQueueRequest) throws AWSException {
		AmazonSQSClient awsClient = new AmazonSQSClient(awsCredentials);
		return new CreateAWSSQSQueueResponse(awsClient.createQueue(createQueueRequest));
	}

	@Override
	public GetAWSSQSQueueAttributesResponse getQueueAttributes(GetAWSSQSQueueAttributesRequest request) throws AWSException {
		AmazonSQSClient awsClient = new AmazonSQSClient(awsCredentials);
		return new GetAWSSQSQueueAttributesResponse(awsClient.getQueueAttributes(request));
	}

	@Override
	public ListAWSSQSQueuesResponse getListOfExistingQueues() throws AWSException {
		AmazonSQSClient awsClient = new AmazonSQSClient(awsCredentials);
		return new ListAWSSQSQueuesResponse(awsClient.listQueues());
	}
}
