package com.deposco.ias.brokers.aws;

import java.util.List;

public interface AWSSESBroker {

	public void sendEmails(String subject, String body, List<String> destinationAddresses, String fromAddress) throws AWSException;

}
