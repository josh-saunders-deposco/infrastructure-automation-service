package com.deposco.ias.brokers.aws.impl;

import java.util.List;

import org.springframework.stereotype.Component;

import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.simpleemail.AmazonSimpleEmailServiceClient;
import com.amazonaws.services.simpleemail.model.Body;
import com.amazonaws.services.simpleemail.model.Content;
import com.amazonaws.services.simpleemail.model.Destination;
import com.amazonaws.services.simpleemail.model.Message;
import com.amazonaws.services.simpleemail.model.SendEmailRequest;
import com.deposco.ias.brokers.aws.AWSBaseBroker;
import com.deposco.ias.brokers.aws.AWSException;
import com.deposco.ias.brokers.aws.AWSSESBroker;

@Component
public class AWSSESBrokerImpl extends AWSBaseBroker implements AWSSESBroker {

	@Override
	public void sendEmails(String subjectAsString, String bodyAsHtml, List<String> destinationAddresses, String fromAddress) throws AWSException {
		try {
			Destination destination = new Destination().withToAddresses(destinationAddresses);
			Content subject = new Content().withData(subjectAsString);
			Content htmlBody = new Content().withData(bodyAsHtml);
			Body body = new Body().withHtml(htmlBody);
			Message message = new Message().withSubject(subject).withBody(body);
			SendEmailRequest request = new SendEmailRequest().withSource(fromAddress).withDestination(destination).withMessage(message);
			AmazonSimpleEmailServiceClient client = new AmazonSimpleEmailServiceClient(awsCredentials);
			Region region = Region.getRegion(Regions.US_EAST_1);
			client.setRegion(region);
			client.sendEmail(request);
		}
		catch (Throwable t) {
			throw new AWSException(t);
		}
	}

}
