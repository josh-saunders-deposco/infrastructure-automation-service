package com.deposco.ias.brokers.aws;

import com.deposco.ias.model.aws.CreateAWSUserAccessKeyRequest;
import com.deposco.ias.model.aws.CreateAWSUserAccessKeyResponse;
import com.deposco.ias.model.aws.CreateAWSUserPolicyRequest;
import com.deposco.ias.model.aws.CreateAWSUserRequest;
import com.deposco.ias.model.aws.CreateAWSUserResponse;

public interface AWSUserBroker {

	public CreateAWSUserResponse createUser(CreateAWSUserRequest request) throws AWSException;

	public CreateAWSUserResponse createUserIfNeeded(CreateAWSUserRequest request) throws AWSException;

	public void createUserPolicyIfNeeded(CreateAWSUserPolicyRequest request) throws AWSException;

	public CreateAWSUserAccessKeyResponse createAccessKeyForUser(CreateAWSUserAccessKeyRequest request) throws AWSException;
}
