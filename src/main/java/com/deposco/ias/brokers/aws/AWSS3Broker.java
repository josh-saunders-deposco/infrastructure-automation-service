package com.deposco.ias.brokers.aws;

import java.io.File;

public interface AWSS3Broker {

	public void createFileAtPath(String bucketName, File file, String remoteFilePath, boolean isPublic) throws AWSException;

	public void populateFileUsingFileAtPath(String bucketName, String s3FilePath, File dest) throws AWSException;
}
