package com.deposco.ias.brokers.aws;

import com.deposco.ias.model.aws.CreateAWSSQSQueueRequest;
import com.deposco.ias.model.aws.CreateAWSSQSQueueResponse;
import com.deposco.ias.model.aws.GetAWSSQSQueueAttributesRequest;
import com.deposco.ias.model.aws.GetAWSSQSQueueAttributesResponse;
import com.deposco.ias.model.aws.ListAWSSQSQueuesResponse;

public interface AWSSQSBroker {

	public CreateAWSSQSQueueResponse createQueue(CreateAWSSQSQueueRequest createQueueRequest) throws AWSException;

	public GetAWSSQSQueueAttributesResponse getQueueAttributes(GetAWSSQSQueueAttributesRequest request) throws AWSException;

	public ListAWSSQSQueuesResponse getListOfExistingQueues() throws AWSException;
}
