package com.deposco.ias.brokers.aws;

public class AWSException extends Exception {

	public AWSException(Throwable t) {
		super(t);
	}

	public AWSException(String message, Throwable t) {
		super(message, t);
	}

}
