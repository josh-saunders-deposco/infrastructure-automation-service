package com.deposco.ias.brokers.aws.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.amazonaws.services.identitymanagement.AmazonIdentityManagementClient;
import com.amazonaws.services.identitymanagement.model.GetUserPolicyRequest;
import com.amazonaws.services.identitymanagement.model.GetUserPolicyResult;
import com.amazonaws.services.identitymanagement.model.GetUserRequest;
import com.amazonaws.services.identitymanagement.model.GetUserResult;
import com.amazonaws.services.identitymanagement.model.NoSuchEntityException;
import com.amazonaws.services.identitymanagement.model.User;
import com.deposco.ias.brokers.aws.AWSBaseBroker;
import com.deposco.ias.brokers.aws.AWSException;
import com.deposco.ias.brokers.aws.AWSUserBroker;
import com.deposco.ias.model.aws.CreateAWSUserAccessKeyRequest;
import com.deposco.ias.model.aws.CreateAWSUserAccessKeyResponse;
import com.deposco.ias.model.aws.CreateAWSUserPolicyRequest;
import com.deposco.ias.model.aws.CreateAWSUserRequest;
import com.deposco.ias.model.aws.CreateAWSUserResponse;

@Component
public class AWSUserBrokerImpl extends AWSBaseBroker implements AWSUserBroker {

	private static final Logger LOG = LoggerFactory.getLogger(AWSUserBrokerImpl.class);

	@Override
	public CreateAWSUserResponse createUser(CreateAWSUserRequest request) throws AWSException {

		try {
			AmazonIdentityManagementClient awsClient = new AmazonIdentityManagementClient(awsCredentials);
			return new CreateAWSUserResponse(awsClient.createUser(request));
		}
		catch (NoSuchEntityException e) {
			return null;
		}
	}

	@Override
	public void createUserPolicyIfNeeded(CreateAWSUserPolicyRequest request) throws AWSException {
		LOG.info("createUserPolicyIfNeeded() :: " + request);
		AmazonIdentityManagementClient awsClient = new AmazonIdentityManagementClient(awsCredentials);

		try {
			GetUserPolicyRequest getRequest = new GetUserPolicyRequest();
			getRequest.setRequestCredentials(awsCredentials);
			getRequest.setUserName(request.getUserName());
			getRequest.setPolicyName(request.getPolicyName());
			GetUserPolicyResult getResult = awsClient.getUserPolicy(getRequest);
			LOG.info("createUserPolicyIfNeeded() :: policy already exists, nothing to do");
		}
		catch (NoSuchEntityException e) {
			LOG.info("createUserPolicyIfNeeded() :: creating policy");
			awsClient.putUserPolicy(request);
			LOG.info("createUserPolicyIfNeeded() :: policy created");
		}
	}

	@Override
	public CreateAWSUserAccessKeyResponse createAccessKeyForUser(CreateAWSUserAccessKeyRequest request) throws AWSException {
		AmazonIdentityManagementClient awsClient = new AmazonIdentityManagementClient(awsCredentials);
		return new CreateAWSUserAccessKeyResponse(awsClient.createAccessKey(request));
	}

	@Override
	public CreateAWSUserResponse createUserIfNeeded(CreateAWSUserRequest request) throws AWSException {
		LOG.info("createUserIfNeeded() :: username :: " + request.getUserName());

		try {
			AmazonIdentityManagementClient awsClient = new AmazonIdentityManagementClient(awsCredentials);
			GetUserRequest getUserRequest = new GetUserRequest();
			getUserRequest.setUserName(request.getUserName());
			GetUserResult result = awsClient.getUser(getUserRequest);
			User user = result.getUser();
			LOG.info("createUserIfNeeded() :: user already existed :: " + user);
			return new CreateAWSUserResponse(user);
		}
		catch (NoSuchEntityException e) {
			LOG.info("createUserIfNeeded() :: user didn't exist, creating now");
			return this.createUser(request);
		}
	}

}
