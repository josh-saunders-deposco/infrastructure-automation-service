package com.deposco.ias.brokers;

import java.util.Date;
import java.util.List;

import com.deposco.ias.model.LogEntry;

public interface LogQueryBroker {

	public List<LogEntry> queryLogs(Date timestamp, int timeWindowInMinutes, String queryPhrase, String logLevel, String[] logEnvironments, int numberOfLogEntries, String logType)
			throws LogQueryException;
}
